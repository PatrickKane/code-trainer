const produceCodes = [
    {
	name: "Broccoli Crowns",
	unit: "S",
	code: "4548"
    },
    {
	name: "Brussel Sprouts",
	unit: "S",
	code: "4550"
    },
    {
	name: "Cabbage-Green",
	unit: "S",
	code: "4069"
    },
    {
	name: "Cabbage-Red",
	unit: "S",
	code: "4554"
    },
    {
	name: "Carrot-Loose",
	unit: "S",
	code: "4562"
    },
    {
	name: "Celery",
	unit: "L",
	code: "4583"
    },
    {
	name: "Chili-Jalapeno",
	unit: "S",
	code: "4693"
    },
    {
	name: "Chili-Serrano",
	unit: "S",
	code: "4709"
    },
    {
	name: "Corn-White",
	unit: "L",
	code: "4077"
    },
    {
	name: "Garlic-Loose",
	unit: "L",
	code: "4608"
    },
    {
	name: "Ginger Root",
	unit: "S",
	code: "4612"
    },
    {
	name: "Lettuce-Green Leaf",
	unit: "L",
	code: "4076"
    },
    {
	name: "Lettuce-Iceberg",
	unit: "L",
	code: "4061"
    },
    {
	name: "Lettuce-Red Leaf",
	unit: "L",
	code: "4075"
    },
    {
	name: "Lettuce-Romaine",
	unit: "L",
	code: "4640"
    },
    {
	name: "Limes",
	unit: "L",
	code: "4048"
    },
    {
	name: "Potato-Red",
	unit: "S",
	code: "4073"
    },
    {
	name: "Potato-Russet Loose",
	unit: "S",
	code: "4072"
    },
    {
	name: "Potato-Sweet",
	unit: "S",
	code: "4091"
    },
    {
	name: "Potato-White",
	unit: "S",
	code: "4083"
    },
    {
	name: "Squash-Italian",
	unit: "S",
	code: "4067"
    },
    {
	name: "Squash-Yellow",
	unit: "S",
	code: "4784"
    },
    {
	name: "Tomatillos",
	unit: "S",
	code: "4801"
    },
    {
	name: "Tomatoes-Roma",
	unit: "S",
	code: "4816"
    },
];

// how to select a random element from the array
// produceCodes[Math.floor(Math.random() * produceCodes.length)].name
const form = document.getElementById('form');
const item = document.getElementById('item');
const wrong = document.getElementById('wrong');

const btn1 = document.getElementById('btn1');
const btn2 = document.getElementById('btn2');
const btn3 = document.getElementById('btn3');
const btn4 = document.getElementById('btn4');
const btn5 = document.getElementById('btn5');
const btn6 = document.getElementById('btn6');
const btn7 = document.getElementById('btn7');
const btn8 = document.getElementById('btn8');
const btn9 = document.getElementById('btn9');
const btn0 = document.getElementById('btn0');
const btnClear = document.getElementById('clear');

let produce = getProduce();

console.log(produce);

item.innerText = produce.name;

form.onsubmit = submit;

function submit(e) {
    e.preventDefault();
    if (form.code.value === produce.code) {
	console.log(form.code.value);
	let oldProduce = produce;
	produce = getProduce();
	console.log(produce);
	item.innerText = produce.name;
	form.code.value = '';
	//wrong.innerText = '';
	form.code.placeholder = 'Correct';
    } else {
	console.log("Incorrect");
	form.code.value = '';
	//wrong.innerText = 'Incorrect';
	form.code.placeholder = 'Incorrect';
    }
};

function getProduce() {
    return produceCodes[Math.floor(Math.random() * produceCodes.length)];
}

btn1.addEventListener('click', function() {
    form.code.value = form.code.value + 1;
});
btn2.addEventListener('click', function() {
    form.code.value = form.code.value + 2;
});
btn3.addEventListener('click', function() {
    form.code.value = form.code.value + 3;
});
btn4.addEventListener('click', function() {
    form.code.value = form.code.value + 4;
});
btn5.addEventListener('click', function() {
    form.code.value = form.code.value + 5;
});
btn6.addEventListener('click', function() {
    form.code.value = form.code.value + 6;
});
btn7.addEventListener('click', function() {
    form.code.value = form.code.value + 7;
});
btn8.addEventListener('click', function() {
    form.code.value = form.code.value + 8;
});
btn9.addEventListener('click', function() {
    form.code.value = form.code.value + 9;
});
btn0.addEventListener('click', function() {
    form.code.value = form.code.value + 0;
});

btnClear.addEventListener('click', function() {
    form.code.value = '';
});
