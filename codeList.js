const produceCodes = [
    {
	name: "Broccoli Crowns",
	unit: "S",
	code: "4548"
    },
    {
	name: "Brussel Sprouts",
	unit: "S",
	code: "4550"
    },
    {
	name: "Cabbage-Green",
	unit: "S",
	code: "4069"
    },
    {
	name: "Cabbage-Red",
	unit: "S",
	code: "4554"
    },
    {
	name: "Carrot-Loose",
	unit: "S",
	code: "4562"
    },
    {
	name: "Celery",
	unit: "L",
	code: "4583"
    },
    {
	name: "Chili-Jalapeno",
	unit: "S",
	code: "4693"
    },
    {
	name: "Chili-Serrano",
	unit: "S",
	code: "4709"
    },
    {
	name: "Corn-White",
	unit: "L",
	code: "4077"
    },
    {
	name: "Garlic-Loose",
	unit: "L",
	code: "4608"
    },
    {
	name: "Ginger Root",
	unit: "S",
	code: "4612"
    },
    {
	name: "Lettuce-Green Leaf",
	unit: "L",
	code: "4076"
    },
    {
	name: "Lettuce-Iceberg",
	unit: "L",
	code: "4061"
    },
    {
	name: "Lettuce-Red Leaf",
	unit: "L",
	code: "4075"
    },
    {
	name: "Lettuce-Romaine",
	unit: "L",
	code: "4640"
    },
    {
	name: "Limes",
	unit: "L",
	code: "4048"
    },
    {
	name: "Potato-Red",
	unit: "S",
	code: "4073"
    },
    {
	name: "Potato-Russet Loose",
	unit: "S",
	code: "4072"
    },
    {
	name: "Potato-Sweet",
	unit: "S",
	code: "4091"
    },
    {
	name: "Potato-White",
	unit: "S",
	code: "4083"
    },
    {
	name: "Squash-Italian",
	unit: "S",
	code: "4067"
    },
    {
	name: "Squash-Yellow",
	unit: "S",
	code: "4784"
    },
    {
	name: "Tomatillos",
	unit: "S",
	code: "4801"
    },
    {
	name: "Tomatoes-Roma",
	unit: "S",
	code: "4816"
    },
];

export produceCodes;
